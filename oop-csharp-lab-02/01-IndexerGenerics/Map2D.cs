﻿using System;
using System.Collections.Generic;

namespace IndexerGenerics
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        /*
         * Suggerimento: come struttura dati interna per la memorizzazione dei valori della mappa
         * si suggerisce di utilizzare un Dictionary generico contenente come chiave una tupla di due
         * valori (le chiavi della mappa) e come valore il valore della mappa.
         * 
         * Esempio:
         * private IDictionary<Tuple<TKey1,TKey2>,TValue> values;
         */

        private IDictionary<Tuple<TKey1, TKey2>, TValue> values;

        public Map2D()
        {
            values = new Dictionary<Tuple<TKey1, TKey2>, TValue>();
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            /*
             * Nota: si verifichi il funzionamento del metodo Invoke() su oggetti di classe Func
             */
            foreach(var key1 in keys1) 
                foreach(var key2 in keys2)
                    values.Add(new Tuple<TKey1, TKey2>(key1, key2), generator.Invoke(key1, key2));
        }

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            return this.GetElements() == other.GetElements();
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            /*
             * Suggerimento: si noti che sulla classe Dictionary è definito un indicizzatore per i valori di chiave
             */

            get => values[new Tuple<TKey1, TKey2>(key1, key2)];

            set => values[new Tuple<TKey1, TKey2>(key1, key2)] = value;
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            IList<Tuple<TKey2, TValue>> row = new List<Tuple<TKey2, TValue>>();
            foreach(var entry in values)
            {
                if (entry.Key.Item1.Equals(key1))
                    row.Add(new Tuple<TKey2, TValue>(entry.Key.Item2, entry.Value));
            }
            return row;
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            IList<Tuple<TKey1, TValue>> column = new List<Tuple<TKey1, TValue>>();
            foreach (var entry in values)
            {
                if (entry.Key.Item2.Equals(key2))
                    column.Add(new Tuple<TKey1, TValue>(entry.Key.Item1, entry.Value));
            }
            return column;
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            IList<Tuple<TKey1, TKey2, TValue>> elements = new List<Tuple<TKey1, TKey2, TValue>>();
            foreach(var entry in elements)
            {
                elements.Add(new Tuple<TKey1, TKey2, TValue>(entry.Item1, entry.Item2, entry.Item3));
            }
            return elements;
        }

        public int NumberOfElements
        {
            get
            {
                return this.values.Count;
            }
        }

        public override string ToString()
        {
            string str = "";

            foreach(var entry in this.values)
                str += "key: " + entry.Key.ToString() + " value: " + entry.Value.ToString() + "\n";

            return str;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
