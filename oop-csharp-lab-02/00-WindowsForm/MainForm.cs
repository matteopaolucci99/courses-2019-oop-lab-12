﻿using System;
using System.Windows.Forms;

namespace _00_WindowsForm
{
    public partial class MainForm : Form
    {
        /*
         * == README ==
         * 
         * Scopo dell'esercizio è comprendere il meccanismo di costruzione di GUI
         * in C#.
         * 
         * 1) Si analizzi il codice generato e si comprenda come VisualStudio organizza
         * il codice delle GUI (in particolare, MainForm.cs e MainForm.Designer.cs)
         * 
         * 2) Si completi il codice dell'evento associato al bottone, con l'obiettivo
         * che i dati (nome e cognome) inseriti nei due campi di testo producano una stringa
         * "aggiunto nome cognome" da appendere al testo presente nella textarea di output.
         * 
         * 3) OPZIONALE: si produca la stessa versione di questo esempio in un altro progetto
         * avvalendosi della suite WPF.
         */

        public MainForm()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            textBox3.AppendText("aggiunto " + textBox1.Text + " " + textBox2.Text + "\n");
        }

        private void MainForm_Load(object sender, EventArgs e)
        {


        }
    }
}
